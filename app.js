const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('./config');

const routes = require('./routes/index');
const users = require('./routes/users');

const app = express();

require('./lib/mongo');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', routes);
app.use('/users', users);
app.use('/projects', require('./routes/projects'));




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// Error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    status: err.status,
    message: err.message,
    error: app.get('env') === 'development' ? err : {}
  });
});

module.exports = app;

app.listen(config.app.port, err => {
  if (err) {
    console.log(err);
  }
  console.log('Server is started on port: ' + config.app.port);
})
