const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();

const Project = mongoose.model('Project');

router
  .get('/', (req, res, next) => {
    Project.find((err, result) => (err ? next(err) : res.json(result)));
  })
  .get('/:id', (req, res, next) => {
    Project.findOne({ _id: req.params.id }, (err, result) => (err ? next(err) : res.json(result)));
  })
  .post('/', (req, res, next) => {
    Project.create(req.body, (err, result) => (err ? next(err) : res.json(result)));
  });

module.exports = router;
