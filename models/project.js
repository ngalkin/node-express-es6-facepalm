const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  title: String,
  isFinished: { type: Boolean, default: false }
});

mongoose.model('Project', schema);
