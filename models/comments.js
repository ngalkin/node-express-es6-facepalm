const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  text: String,
  created: { type: Date, default: Date.now }
});

mongoose.model('Comment', schema);
